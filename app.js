const express = require('express')

const app = express()

// variables d'env
const PORT = process.env.SERVER_PORT || 3000;


// endpoints route

app.get('/', function (req, res) {
res.send('Hello World')
})

app.get('/help', function (req, res) {
res.send('Help!')
})


app.listen(PORT, () => {
console.log('Server running on port ' + PORT + '...')
})
